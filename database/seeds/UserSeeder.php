<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Ali',
                'email' => 'js1@demo.com',
                'password' => bcrypt('aaaaaa'),
                'role' => 'Job Seekers'
            ],
            [
                'name' => 'Akau',
                'email' => 'js2@demo.com',
                'password' => bcrypt('aaaaaa'),
                'role' => 'Job Seekers'
            ],
            [
                'name' => 'Muthu',
                'email' => 'js3@demo.com',
                'password' => bcrypt('aaaaaa'),
                'role' => 'Job Seekers'
            ],
            [
                'name' => 'ABC Company',
                'email' => 'hh1@demo.com',
                'password' => bcrypt('aaaaaa'),
                'role' => 'Headhunters'
            ],
            [
                'name' => 'DEF Company',
                'email' => 'hh2@demo.com',
                'password' => bcrypt('aaaaaa'),
                'role' => 'Headhunters'
            ],
            [
                'name' => 'GHI Company',
                'email' => 'hh3@demo.com',
                'password' => bcrypt('aaaaaa'),
                'role' => 'Headhunters'
            ],
        ]);

    }
}
