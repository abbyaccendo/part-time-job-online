<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobpostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobposts')->insert([
            [
                'title' => 'Promoter',
                'description' => 'super super easy job ',
                'working_hours' => '0900-1800',
                'dress_code' => 'semi-formal',
                'language' => 'English',
                'salary' => 'RM8/hour',
                'state' => 'Selangor',
                'location' => 'Jalan Impian, Taman Impian, 88888 Selangor, Malaysia',
                'headhunter_id' => 1
            ],
            [
                'title' => 'Skincare Consultant',
                'description' => 'super duper easy job ',
                'working_hours' => '0900-1800',
                'dress_code' => 'semi-formal',
                'language' => 'Tamil',
                'salary' => 'RM8/hour',
                'state' => 'Selangor',
                'location' => 'Jalan Impian, Taman Impian, 88888 Selangor, Malaysia',
                'headhunter_id' => 1
            ],
            [
                'title' => 'Customer Service',
                'description' => 'money money come job ',
                'working_hours' => '0900-1800',
                'dress_code' => 'mascot',
                'language' => 'Chinese',
                'salary' => 'RM8/hour',
                'state' => 'Selangor',
                'location' => 'Jalan Impian, Taman Impian, 88888 Selangor, Malaysia',
                'headhunter_id' => 1
            ],
            [
                'title' => 'Skincare Consultant',
                'description' => 'super super easy job ',
                'working_hours' => '0900-1800',
                'dress_code' => 'formal',
                'language' => 'English',
                'salary' => 'RM10/hour',
                'state' => 'Kuala Lumpur',
                'location' => 'Jalan Happy, Happy Happy, 88888 Kuala Lumpur, Malaysia',
                'headhunter_id' => 2
            ],
            [
                'title' => 'Customer Service',
                'description' => 'super super super easy job ',
                'working_hours' => '0900-1800',
                'dress_code' => 'mascot',
                'language' => 'Chinese',
                'salary' => 'RM10/hour',
                'state' => 'Kuala Lumpur',
                'location' => 'Jalan Happy, Happy Happy, 88888 Kuala Lumpur, Malaysia',
                'headhunter_id' => 2
            ],
            [
                'title' => 'Promoter',
                'description' => 'super super double easy job ',
                'working_hours' => '0900-1800',
                'dress_code' => 'casual',
                'language' => 'Malay',
                'salary' => 'RM10/hour',
                'state' => 'Johor',
                'location' => 'Jalan TuanTuan, Happy YuanYuan, 11111 Johor, Malaysia',
                'headhunter_id' => 3
            ],
            [
                'title' => 'Promoter',
                'description' => 'super super triple easy job ',
                'working_hours' => '0900-1800',
                'dress_code' => 'casual',
                'language' => 'Malay',
                'salary' => 'RM10/hour',
                'state' => 'Johor',
                'location' => 'Jalan TuanTuan, Happy YuanYuan, 11111 Johor, Malaysia',
                'headhunter_id' => 3
            ],

        ]);

    }
}
