<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeadhunterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('headhunters')->insert([
            [
                'company_telephone' => '0344444444',
                'company_address' => 'Jalan Ali Baba, Taman Ali Baba 82000 Johor, Malaysia',
                'company_website' => 'companyabc.com.my',
                'user_id' => 4
            ],
            [
                'company_telephone' => '0355555555',
                'company_address' => 'Jalan Ali Abu, Taman Ali Abu 62000 Selangor, Malaysia',
                'company_website' => 'companydef.com.my',
                'user_id' => 5
            ],
            [
                'company_telephone' => '0366666666',
                'company_address' => 'Jalan Ali Muthu, Taman Ali Muthu 82000 Kuala Lumpur, Malaysia',
                'company_website' => 'companyghi.com.my',
                'user_id' => 6
            ],
        ]);

    }
}
