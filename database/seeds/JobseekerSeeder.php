<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobseekerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobseekers')->insert([
            [
                'telephone' => '0101111111',
                'gender' => 'Male',
                'experience' => 'Experience: promoter, accountant, sampling, driver and bla bla bla',
                'dateOfBirth' => '1997-08-31 16:46:22',
                'user_id' => 1
            ],
            [
                'telephone' => '0102222222',
                'gender' => 'Female',
                'experience' => 'Experience: promoter ba ba ba ba naaaaaa bla bla bla',
                'dateOfBirth' => '1963-08-31 16:46:22',
                'user_id' => 2
            ],
            [
                'telephone' => '0103333333',
                'gender' => 'Male',
                'experience' => 'Experience: promoddfedf erfeiwrtfhe wihfewirhiwqe rhiwqa',
                'dateOfBirth' => '1983-08-31 16:46:22',
                'user_id' => 3
            ],
        ]);

    }
}
