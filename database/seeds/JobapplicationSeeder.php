<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobapplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_applications')->insert([
            [
                'jobseeker_id' => '1',
                'job_post_id' => '1',
                'status' => 'pending'
            ],
            [
                'jobseeker_id' => '2',
                'job_post_id' => '1',
                'status' => 'pending'
            ],
            [
                'jobseeker_id' => '3',
                'job_post_id' => '1',
                'status' => 'pending'
            ],
            [
                'jobseeker_id' => '1',
                'job_post_id' => '2',
                'status' => 'pending'
            ],
            [
                'jobseeker_id' => '2',
                'job_post_id' => '2',
                'status' => 'pending'
            ],
            [
                'jobseeker_id' => '3',
                'job_post_id' => '2',
                'status' => 'pending'
            ],
            [
                'jobseeker_id' => '1',
                'job_post_id' => '3',
                'status' => 'pending'
            ],
            [
                'jobseeker_id' => '2',
                'job_post_id' => '3',
                'status' => 'pending'
            ],
        ]);

    }
}
