<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(JobseekerSeeder::class);
         $this->call(HeadhunterSeeder::class);
         $this->call(JobpostSeeder::class);
         $this->call(JobapplicationSeeder::class);
    }
}
