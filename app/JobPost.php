<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPost extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','description', 'working_hours','dress_code','language', 'salary', 'headhunter_id'
    ];

    protected $table = 'jobposts';

    public function headhunter()
    {
        return $this->belongsTo(Headhunter::class);
    }

    public function jobapplications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tags_jobposts');
    }

}
