<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Jobseeker extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'telephone','gender', 'experience','dateOfBirth'
    ];

    protected $table = 'jobseekers';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['dateOfBirth'];

    public function getAgeAttribute()
    {
        return Carbon::now()->diffInYears($this->dateOfBirth);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function jobapplications()
    {
        return $this->hasMany(JobApplication::class);
    }

    function getDobAttribute()
    {
        return $this->dateOfBirth->toDateString();
    }

}
