<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jobseeker_id','job_post_id','status'
    ];

    protected $table = 'job_applications';

    public function jobseeker()
    {
        return $this->belongsTo(Jobseeker::class);
    }

    public function job_post()
    {
        return $this->belongsTo(JobPost::class);
    }

}
