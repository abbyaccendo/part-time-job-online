<?php

namespace App\Http\Controllers;

use App\Headhunter;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewHeadhunterProfile(Headhunter $headhunter)
    {
        return view('headhunter.profile', compact('headhunter'));
    }

    public function editProfile(User $user)
    {
        return view('editProfile', compact('user'));
    }

    public function updateProfile(Request $request, User $user)
    {
        $user->name = $request['name'];
        $user->email = $request['email'];
        if($user->role == "Headhunters")
        {
            $headhunter = $user->headhunter;
            $headhunter->company_telephone = $request['phone_number'];
            $headhunter->company_address = $request['company_address'];
            $headhunter->company_website = $request['company_website'];
            $headhunter->save();
        }
        else
        {
            $jobseeker = $user->jobseeker;
            $jobseeker->gender = $request['gender'];
            $jobseeker->dateOfBirth = $request['dob'];
            $jobseeker->experience = $request['experience'];
            $jobseeker->telephone = $request['phone_number'];
            $jobseeker->save();
        }
        $user->save();
        return back()->with('user')->with('status', 'Edited profile successfully!');
    }

    public function changePassword()
    {
        return view('auth.passwords.changePassword');
    }

    public function storeNewPassword(Request $request)
    {
        $this->validate(request(), [
            'password' => 'required|string|min:6|confirmed',
        ]);
        $originalPassword = "";
        try{
            $user = Auth::user();
            $originalPassword = $user->password;
            $user->password = bcrypt($request['password']);
            $user->save();
            return redirect()->route('home')->with('status', 'Changed password successfully!');
        }
        catch(Throwable $errors)
        {
            $user->password = bcrypt($originalPassword);
            return back()->with('status', 'Fail to change password!');
        }


    }
}

