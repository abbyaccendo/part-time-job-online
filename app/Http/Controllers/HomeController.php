<?php

namespace App\Http\Controllers;

use App\JobPost;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobposts = JobPost::latest()->get()->map(function ($jobpost) {
            return [
                'id' => $jobpost->id,
                'title' => $jobpost->title,
                'description' => $jobpost->description,
                'working_hours' => $jobpost->working_hours,
                'dress_code' => $jobpost->dress_code,
                'language' => $jobpost->language,
                'salary' => $jobpost->salary,
                'location' => $jobpost->location,
                'state' => $jobpost->state,
                'headhunter_id' => $jobpost->headhunter->id,
                'headhunter_name' => $jobpost->headhunter->user->name
            ];
        });
        $allTitles = DB::table('jobposts')->distinct()->pluck('title');
        $allStates = DB::table('jobposts')->distinct()->pluck('state');
        return view('home', compact('jobposts','allTitles','allStates'));
    }
}
