<?php

namespace App\Http\Controllers\Auth;

use App\Headhunter;
use App\Jobseeker;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'telephone' => 'required',
            'role' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->role = $data['role'];
        $user->password = bcrypt($data['password']);
        $user->save();

        if ($data['role'] == 'Job Seekers') {
            $jobseeker = new Jobseeker;
            $jobseeker->telephone = $data['telephone'];
            $jobseeker->gender = $data['gender'];
            $jobseeker->dateOfBirth = $data['dob'];
            $jobseeker->experience = $data['experience'];
            $user->jobseeker()->save($jobseeker);
            $jobseeker->save();
        } else {
            $headhunter = new Headhunter;
            $headhunter->company_telephone = $data['telephone'];
            $headhunter->company_address = $data['company-address'];
            $headhunter->company_website = $data['company-website'];
            $user->headhunter()->save($headhunter);
            $headhunter->save();
        }

        return $user;
    }
}
