<?php

namespace App\Http\Controllers;

use App\Headhunter;
use App\JobApplication;
use App\JobPost;
use App\Jobseeker;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;


class PostJobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCreateJobForm()
    {
        return view('headhunter.createJobPost');
    }

    public function createJobPost(Request $request)
    {
        $this->validate($request, [
            'position-title' => 'required',
            'job-location-state' => 'required',
            'job-location-exact' => 'required',
            'dresscode' => 'required',
            'language' => 'required',
            'salary-money' => 'required',
            'salary-period' => 'required',
            'working-hours' => 'required',
            'job-description' => 'required'
        ]);

        $headhunter = Headhunter::where('user_id', Auth::user()->id)->first();

        $jobpost = new JobPost;
        $jobpost->title = $request['position-title'];
        $jobpost->description = $request['job-description'];
        $jobpost->working_hours = $request['working-hours'];
        $jobpost->dress_code = $request['dresscode'];
        $jobpost->location = $request['job-location-exact'];
        $jobpost->state = $request['job-location-state'];
        $jobpost->salary = 'RM ' . $request['salary-money'] . '/' . $request['salary-period'];
        $jobpost->language = $request['language'];
        $headhunter->jobposts()->save($jobpost);
        return redirect()->route('viewJobPost', ['jobpost' => $jobpost->id])->with('status', 'Created job successfully!');

    }

    public function viewJobPost(JobPost $jobpost)
    {
        $headhunter = $jobpost->headhunter;
        if(Auth::user()->jobseeker)
            $applied = JobApplication::where('jobseeker_id',Auth::user()->jobseeker->id)->where('job_post_id',$jobpost->id)->count();
        else
            $applied = -1;
        return view('singleJobPost', compact('jobpost', 'headhunter','applied'));
    }

    public function applyJob(JobPost $jobpost)
    {
        if (Auth::user()->jobseeker) {
            try
            {
                $application = new JobApplication;
                $application->jobseeker_id = Auth::user()->jobseeker->id;
                $application->job_post_id = $jobpost->id;
                $application->save();
            }
            catch(Throwable $errors)
            {
                $application->delete();
                return back()->with('status', 'You have already applied this job!');
            }
        }
        return back()->with('status', 'Applied job successfully!');
    }

    public function viewMyPostedJob(Headhunter $headhunter)
    {
        $jobposts = $headhunter->jobposts;
        return view('headhunter.viewMyPostedJob', compact('jobposts'));
    }

    public function acceptApplication(JobPost $jobpost, Jobseeker $jobseeker)
    {
        JobApplication::where('jobseeker_id',$jobseeker->id)
            ->where('job_post_id',$jobpost->id)->update(['status' => 'accepted']);
        return back()->with('status', 'You have accepted an application!');

    }

    public function rejectApplication(JobPost $jobpost, Jobseeker $jobseeker)
    {
        JobApplication::where('jobseeker_id',$jobseeker->id)
            ->where('job_post_id',$jobpost->id)->update(['status' => 'rejected']);
        return back()->with('status', 'You have rejected an application!');
    }

    public function viewMyAppliedJob(Jobseeker $jobseeker)
    {
        $jobapplications = $jobseeker->jobapplications;
        return view('jobseeker.viewMyAppliedJob', compact('jobapplications'));
    }

    public function searchByTags(Request $request)
    {
        $jobposts = JobPost::whereIn('title',request('selectedTags'))->orWhereIn('state',request('selectedTags'))->get()->map(function ($jobpost) {
            return [
                'id' => $jobpost->id,
                'title' => $jobpost->title,
                'description' => $jobpost->description,
                'working_hours' => $jobpost->working_hours,
                'dress_code' => $jobpost->dress_code,
                'language' => $jobpost->language,
                'salary' => $jobpost->salary,
                'location' => $jobpost->location,
                'state' => $jobpost->state,
                'headhunter_id' => $jobpost->headhunter->id,
                'headhunter_name' => $jobpost->headhunter->user->name
            ];
        });
        return $jobposts;
    }

}
