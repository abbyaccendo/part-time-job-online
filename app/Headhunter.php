<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Headhunter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_telephone','company_address', 'company_website'
    ];

    protected $table = 'headhunters';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function jobposts()
    {
        return $this->hasMany(JobPost::class);
    }
}
