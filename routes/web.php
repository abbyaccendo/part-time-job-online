<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        if(Auth::check())
            return redirect('/home');
        else
            return view('auth/login');
    })->name('landingPage');

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => 'auth'], function () {
        //Headhunter menu
        Route::get('/createJobPost', 'PostJobController@showCreateJobForm')->name('showCreateJobForm');
        Route::post('/createJobPost', 'PostJobController@createJobPost')->name('createJobPost');
        Route::get('/viewMyPostedJob/{headhunter}', 'PostJobController@viewMyPostedJob')->name('viewMyPostedJob');
        Route::post('/JobPost/{jobpost}/Applicant/{jobseeker}/Accept', 'PostJobController@acceptApplication')->name('acceptApplication');
        Route::post('/JobPost/{jobpost}/Applicant/{jobseeker}/Reject', 'PostJobController@rejectApplication')->name('rejectApplication');

        //Jobseeker menu
        Route::get('/viewMyAppliedJob/{jobseeker}', 'PostJobController@viewMyAppliedJob')->name('viewMyAppliedJob');

        //Both headhunter and jobseeker
        Route::get('/JobPost/{jobpost}', 'PostJobController@viewJobPost')->name('viewJobPost');
        Route::post('/JobPost/{jobpost}/apply', 'PostJobController@applyJob')->name('applyJob');
        Route::get('/HeadhunterProfile/{headhunter}', 'ProfileController@viewHeadhunterProfile')->name('viewHeadhunterProfile');
        Route::get('/editProfile/{user}', 'ProfileController@editProfile')->name('editProfile');
        Route::put('/editProfile/{user}', 'ProfileController@updateProfile')->name('updateProfile');
        Route::get('/changePassword', 'ProfileController@changePassword')->name('changePassword');
        Route::post('/changePassword', 'ProfileController@storeNewPassword')->name('storeNewPassword');
        Route::post('/searchByTags', 'PostJobController@searchByTags')->name('searchByTags');
    });

});