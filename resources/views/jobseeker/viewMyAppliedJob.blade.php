<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/viewApplication.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <title>View Applied Jobs</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">
                <a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a>
            </span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link" href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link" href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--2-col"></div>
            <div class="mdl-cell mdl-cell--8-col">
                <div class="page-title">
                    <h4>View Applied Jobs</h4>
                </div>

                <div>
                    <ul class="demo-list-two mdl-list apply-list">
                        @foreach($jobapplications as $jobapplication)
                            <li class="mdl-list__item mdl-list__item--two-line">
								<span class="mdl-list__item-primary-content">
									<i class="material-icons mdl-list__item-icon">work</i>
									<a class="job-title"
                                       href="{{route('viewJobPost',['jobpost' => $jobapplication->job_post->id])}}">
                                        {{$jobapplication->job_post->title}}
                                        <i class="material-icons open_in_new">open_in_new</i>
									</a>
									<span class="mdl-list__item-sub-title">{{$jobapplication->job_post->headhunter->user->name}}</span>
								</span>
                                <span class="mdl-list__item-secondary-content">
									<span class="mdl-list__item-secondary-action">
										<div class="mdl-button mdl-js-button mdl-button--raised">
                                                    {{$jobapplication->status}}
                                        </div>
									</span>
								</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </main>
</div>
<script>

</script>
</body>
</html>