<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/editProfile.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <script defer src="/js/data-required.js"></script>
    <title>Edit Profile</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">
                <a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a>
            </span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link" href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link" href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>


    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--3-col"></div>
            <div class="mdl-cell mdl-cell--6-col">
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title card__title">
                        <h2 class="mdl-card__title-text">Edit Profile</h2>
                    </div>
                    <div class="mdl-card__supporting-text text-in-card">



                        <form method="POST" action="">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="name" name="name" value="{{$user->name}}" required>
                                <label class="mdl-textfield__label" for="name">Name</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="email" name="email" value="{{$user->email}}"
                                       required>
                                <label class="mdl-textfield__label" for="email">Email Address</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="tel" pattern="-?[0-9]*(\.[0-9]+)?"
                                       id="phone_number" name="phone_number"
                                       value="{{$user->role=='Headhunters'? $user->headhunter->company_telephone : $user->jobseeker->telephone }}" required>
                                <label class="mdl-textfield__label" for="phone_number">Telephone Number</label>
                                <span class="mdl-textfield__error">Input is not a number!</span>
                            </div>


                            @if($user->role=='Job Seekers')
                            <!-- Job Seekers 1-->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" id="js-1">
                                <input class="mdl-textfield__input" type="text" id="gender" name="gender" value="{{$user->jobseeker->gender}}" required>
                                <label class="mdl-textfield__label" for="gender">Gender</label>
                            </div>
                            <!-- Job Seekers 2-->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
                                 id="js-2">
                                <input class="mdl-textfield__input" name="dob" type="text" value="{{$user->jobseeker->dob}}" onfocus="(this.type='date')" onblur="{this.type='text'}" required>
                                <label class="mdl-textfield__label" for="dob">Date of Birth</label>
                            </div>

                            <!-- Job Seekers 3 -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
                                 id="js-3">
									<textarea class="mdl-textfield__input" type="text" rows="4" id="experience" name="experience" required>
										{{$user->jobseeker->experience}}
									</textarea>
                                <label class="mdl-textfield__label" for="experience">Experience</label>
                            </div>

                            @elseif($user->role == 'Headhunters')
                            <!-- Headhunters 2 -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
                                 id="hh-2">
                                <input class="mdl-textfield__input" type="text" id="company_website" name="company_website"
                                       value="{{$user->headhunter->company_website}}" required>
                                <label class="mdl-textfield__label" for="company_website">Company Website</label>
                            </div>

                            <!-- Headhunters 3 -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
                                 id="hh-3">
                                <input class="mdl-textfield__input" type="text" id="company_address" name="company_address"
                                       value="{{$user->headhunter->company_address}}" required>
                                <label class="mdl-textfield__label" for="company_address">Company Address</label>
                            </div>
                            @endif
                            <!-- change password -->
                            <div class="change-password">
                                <a href="{{route('changePassword')}}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">
                                    Change Password
                                </a>
                            </div>

                            <!-- Submit & Cancel button -->
                            <div class="submit-cancel">
                                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary"
                                        type="submit">
                                    Save
                                </button>

                                <a href="{{route('home')}}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">
                                    Cancel
                                </a>
                            </div>
                        </form>

                        @if (count($errors))
                            <div class="alert alert-danger fade in m-b-15">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="mdl-js-snackbar mdl-snackbar mdl-snackbar--active">
                                <div class="mdl-snackbar__text">
                                    {{ session('status') }}
                                </div>
                                <button class="mdl-snackbar__action" type="button"></button>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="/js/date-input-polyfill.js"></script>
</body>
</html>