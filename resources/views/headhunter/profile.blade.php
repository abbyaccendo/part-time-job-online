<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/viewProfile.css">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <title>View Headhunter Profile</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title"><a class="title-link"
                                              href="{{route('landingPage')}}">Part-time Job Online</a></span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link" href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link" href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>


    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--3-col"></div>
            <div class="mdl-cell mdl-cell--6-col">
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title card__title">
                        <h2 class="mdl-card__title-text">View Headhunter Profile</h2>
                    </div>
                    <div class="mdl-card__supporting-text text-in-card">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="name" value="{{$headhunter->user->name}}" readonly>
                            <label class="mdl-textfield__label" for="first-name">Company Name</label>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="email" value="{{$headhunter->user->email}}" readonly>
                            <label class="mdl-textfield__label" for="email">Email Address</label>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="tel" pattern="-?[0-9]*(\.[0-9]+)?" id="phone_number" value="{{$headhunter->company_telephone}}" readonly>
                            <label class="mdl-textfield__label" for="phone_number">Telephone Number</label>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="website" value="{{$headhunter->company_website}}" readonly>
                            <label class="mdl-textfield__label" for="gender">Company Website</label>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="address" value="{{$headhunter->company_address}}" readonly>
                            <label class="mdl-textfield__label" for="gender">Company Address</label>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
</main>
</div>
</body>
</html>