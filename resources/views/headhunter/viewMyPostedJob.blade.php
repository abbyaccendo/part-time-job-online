<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/viewApplicant.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <title>View Job Applicants</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout" id="allPostedJobContainer">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">
                <a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a>
            </span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link"
                       href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied
                        Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link"
                       href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted
                        Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit
                    Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>


    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--2-col"></div>
            <div class="mdl-cell mdl-cell--8-col">
                <div class="page-title">
                    <h4>View Job Applicants</h4>
                </div>

                @foreach($jobposts as $jobpost)
                    <div id="job_post_{{$jobpost->id}}">
                        <div class="mdl-list__item job-title-list">
                            <span class="mdl-list__item-primary-content">{{$jobpost->title}}</span>
                            <span class="mdl-list__item-secondary-content pointer"
                                  onclick="toggleInfo({{$jobpost->id}})">
								<i class="material-icons">info</i>
							</span>
                            <span class="mdl-list__item-secondary-content pointer"
                                  onclick="toggleApplicantList({{$jobpost->id}})">
								<i class="material-icons">expand_more</i>
							</span>
                        </div>
                        <div id="job_info_{{$jobpost->id}}" style="display:none">
                            <div class="mdl-card__supporting-text">
                                <p><strong>Job Location: </strong>{{$jobpost->location}}</p>
                                <p><strong>Working Hours: </strong>{{$jobpost->working_hours}}</p>
                                <p><strong>Salary: </strong>{{$jobpost->salary}}</p>
                                <p><strong>Language: </strong>{{$jobpost->language}}</p>
                                <p><strong>Dresscode: </strong>{{$jobpost->dress_code}}</p>
                                <p><strong>Description: </strong> {{$jobpost->description}}</p>
                            </div>
                        </div>
                        <div id="job_applicants_{{$jobpost->id}}" class="applicant-list" style="display:none">
                            <ul class="demo-list-icon mdl-list">
                                {{--applicant list--}}
                                @foreach($jobpost->jobapplications as $jobapplication)
                                    <li class="mdl-list__item mdl-list__item--three-line">
                                        <span class="demo-list-width mdl-list__item-primary-content">
                                            <i class="material-icons mdl-list__item-avatar">person</i>
                                            <span>{{$jobapplication->jobseeker->user->name}}</span>
                                            <span class="supporting-details">{{$jobapplication->jobseeker->gender}}</span>
                                            <span class="supporting-details">{{$jobapplication->jobseeker->age}}</span>
                                            <span class="supporting-details">{{$jobapplication->jobseeker->telephone}}</span>
                                            <span class="mdl-list__item-text-body">{{$jobapplication->jobseeker->experience}}</span>
                                        </span>
                                        <span class="mdl-list__item-secondary-content">
                                            <span class="mdl-list__item-secondary-info"></span>
                                            @if($jobapplication->status == "pending" )
                                            <div class="status-bar">
                                                <form action="{{route('acceptApplication',['jobpost' => $jobpost->id, 'jobseeker' => $jobapplication->jobseeker->id])}}"
                                                      method="POST">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button id="accept"
                                                            class="mdl-button mdl-js-button mdl-button--primary mdl-button--raised accept-btn"
                                                            type="submit">
                                                        Accept
                                                    </button>
                                                </form>
                                                <form action="{{route('rejectApplication',['jobpost' => $jobpost->id, 'jobseeker' => $jobapplication->jobseeker->id])}}"
                                                      method="POST">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button id="reject"
                                                            class="mdl-button mdl-js-button mdl-button--primary mdl-button--raised reject-btn"
                                                            type="submit">
                                                        Reject
                                                    </button>
                                                </form>
                                            </div>
                                            @else
                                                <div class="mdl-button mdl-js-button mdl-button--raised">
                                                    {{$jobapplication->status}}
                                                </div>
                                            @endif                                           
									    </span>

                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </main>
    @if (session('status'))
        <div class="mdl-js-snackbar mdl-snackbar mdl-snackbar--active">
            <div class="mdl-snackbar__text">
                {{ session('status') }}
            </div>
            <button class="mdl-snackbar__action" type="button"></button>
        </div>
    @endif
</div>
<script>

    function toggleApplicantList(id) {
        var x = document.getElementById("job_applicants_" + id);
        if (x.style.display !== 'block') {
            x.style.display = 'block';
        }
        else {
            x.style.display = 'none';
        }
    }

    function toggleInfo(id) {
        var x = document.getElementById("job_info_" + id);
        if (x.style.display !== 'block') {
            x.style.display = 'block';
        }
        else {
            x.style.display = 'none';
        }
    }

</script>
</body>
</html>