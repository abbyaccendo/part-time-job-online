<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/postJob.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <link rel="stylesheet" href="/css/getmdl-select.min.css"/>
    <script defer src="/js/getmdl-select.min.js"></script>
    <script defer src="/js/data-required.js"></script>
    <title>Create Job</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title"><a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a></span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link" href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link" href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--3-col"></div>
            <div class="mdl-cell mdl-cell--6-col">
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title card__title">
                        <h2 class="mdl-card__title-text">Create Job</h2>
                    </div>
                    <div class="mdl-card__supporting-text text-in-card">

                        <form class="form-horizontal" method="POST" action="{{ route('createJobPost') }}">
                            {{ csrf_field() }}

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="position-title" name="position-title" required>
                                <label class="mdl-textfield__label" for="position-title">Position Title</label>
                            </div>

                            <!-- Job location State for keyword search purpose-->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth">
                                <input class="mdl-textfield__input" type="text" id="job-location-state" name="job-location-state" required tabIndex="-1" onchange="jobLocation()">
                                <label for="job-location-state">
                                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                </label>
                                <label for="job-location-state" class="mdl-textfield__label">Job Location</label>
                                <ul for="job-location-state" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                    <li class="mdl-menu__item">Kuala Lumpur</li>
                                    <li class="mdl-menu__item">Selangor</li>
                                    <li class="mdl-menu__item">Johor</li>
                                    <li class="mdl-menu__item">Penang</li>
                                    <li class="mdl-menu__item">Kedah</li>
                                    <li class="mdl-menu__item">Kelantan</li>
                                    <li class="mdl-menu__item">Labuan</li>
                                    <li class="mdl-menu__item">Melaka</li>
                                    <li class="mdl-menu__item">Negeri Sembilan</li>
                                    <li class="mdl-menu__item">Pahang</li>
                                    <li class="mdl-menu__item">Perak</li>
                                    <li class="mdl-menu__item">Perlis</li>
                                    <li class="mdl-menu__item">Putrajaya</li>
                                    <li class="mdl-menu__item">Sabah</li>
                                    <li class="mdl-menu__item">Sarawak</li>
                                    <li class="mdl-menu__item">Terengganu</li>
                                </ul>
                            </div>

                            <!-- User input job location -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label location-toggle" id='job-location-exact'>
                                <input class="mdl-textfield__input" type="text" id="job-location-exact" name="job-location-exact" required>
                                <label class="mdl-textfield__label" for="job-location-exact">Exact Location</label>
                            </div>


                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="dresscode" name="dresscode" required>
                                <label class="mdl-textfield__label" for="dresscode">Dresscode</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth">
                                <input class="mdl-textfield__input" type="text" id="language" name="language" tabIndex="-1" required>
                                <label for="language">
                                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                </label>
                                <label for="language" class="mdl-textfield__label">Language</label>
                                <ul for="language" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                    <li class="mdl-menu__item" data-val="EN">English</li>
                                    <li class="mdl-menu__item" data-val="CH">Chinese</li>
                                    <li class="mdl-menu__item" data-val="MY">Malay</li>
                                    <li class="mdl-menu__item" data-val="TM">Tamil</li>
                                    <li class="mdl-menu__item" data-val="OT">Others</li>
                                </ul>
                            </div>

                            <div class="display-block">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label salary">
                                    <input class="mdl-textfield__input salary-align" type="text" pattern="[0-9]{1,25}" id="salary-money" name="salary-money" required>
                                    <label class="mdl-textfield__label" for="salary-money">Salary (RM)</label>
                                    <span class="mdl-textfield__error">Input is not a number!</span>
                                </div>
                                /
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">
                                    <input class="mdl-textfield__input" type="text" id="salary-period" name="salary-period" tabIndex="-1" required>
                                    <label for="salary-period">
                                        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                    </label>
                                    <label for="salary-period" class="mdl-textfield__label">Per period</label>
                                    <ul for="salary-period" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                        <li class="mdl-menu__item" data-val="1">hour</li>
                                        <li class="mdl-menu__item" data-val="2">day</li>
                                        <li class="mdl-menu__item" data-val="3">week</li>
                                        <li class="mdl-menu__item" data-val="4">month</li>
                                        <li class="mdl-menu__item" data-val="5">year</li>
                                        <li class="mdl-menu__item" data-val="6">event</li>
                                    </ul>
                                </div>
                            </div>


                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label display-block">
                                <input class="mdl-textfield__input" type="text" id="working-hours" name="working-hours" required>
                                <label class="mdl-textfield__label" for="working-hours">Working Hours</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <textarea class="mdl-textfield__input" type="text" rows= "4" id="job-description" name="job-description" required></textarea>
                                <label class="mdl-textfield__label" for="job-description">Job Description</label>
                            </div>

                            <!-- Submit button -->
                            <div class="post-btn">
                                {{--<div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" type="submit" onclick="submitValidation()">--}}
                                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" type="submit">
                                Post Job
                                </button>
                                {{--</div>--}}
                            </div>

                        </form>

                        @if (count($errors))
                            <div class="alert alert-danger fade in m-b-15">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
</main>
</div>
<script src="/js/date-input-polyfill.js"></script>
<script>
    // Validation function
    //need to do complete form function also
    function submitValidation(){
        if (document.getElementById("first-name").value != '') {
            alert("HEL");
        }
        else  {
            var d = document.getElementById("first-name");
            d.className += " is-invalid";
        }
    }

    //Job Location
    function jobLocation(){
        document.getElementById("job-location-exact").style.display = 'inline-block';
    }

</script>
</body>
</html>