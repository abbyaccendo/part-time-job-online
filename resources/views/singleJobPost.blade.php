<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/viewJob.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <title>View Job</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title"><a class="title-link"
                                              href="{{route('landingPage')}}">Part-time Job Online</a></span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link" href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link" href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>
    <main class="mdl-layout__content grid-padding">

        <div class="mdl-grid no-bottom-padding">
            <div class="mdl-cell mdl-cell--1-col"></div>

            <div class="mdl-cell mdl-cell--10-col no-bottom-margin">

                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <!-- Job Overview card -->
                    <div class="mdl-card__title job-company">
                        <div>
                            <h2 class="mdl-card__title-text	job-title">{{$jobpost->title}}</h2>

                            {{--<h5 class="company-name">{{$jobpost->title}}</h5>--}}
                            <h5><a class="company-name" href="{{route('viewHeadhunterProfile',['headhunter' => $jobpost->headhunter->id])}}">{{$headhunter->user->name}}</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mdl-grid no-bottom-padding no-top-padding">
            <div class="mdl-cell mdl-cell--1-col"></div>

            <div class="mdl-cell mdl-cell--7-col job-description-margin">
                <!-- Job Description -->
                <div class="demo-card-wide mdl-card mdl-shadow--2dp job-description">
                    <div class="mdl-card__title">
                        <h2 class="mdl-card__title-text">About the Job</h2>
                    </div>
                    <div class="mdl-card__supporting-text">
                        <p><label><strong>Job Location: </strong>{{$jobpost->location}}</label></p>
                        <p><label><strong>Working Hours: </strong>{{$jobpost->working_hours}}</label></p>
                        <p><label><strong>Salary: </strong>{{$jobpost->salary}}</label></p>
                        <p><label><strong>Language: </strong>{{$jobpost->language}}</label></p>
                        <p><label><strong>Dresscode: </strong>{{$jobpost->dress_code}}</label></p>
                        <p><label><strong>Description: </strong> {{$jobpost->description}}</label></p>
                    </div>
                </div>
            </div>
            <div class="mdl-cell mdl-cell--3-col no-left-margin ">
                <!-- Company Details -->
                <div class="demo-card-wide mdl-card mdl-shadow--2dp job-description">
                    <div class="mdl-card__title">
                        <h2 class="mdl-card__title-text">Company Details</h2>
                    </div>
                    <div class="mdl-card__supporting-text">
                        <p><label><strong>Address: </strong>{{$headhunter->company_address}}</label></p>
                        <p><label><strong>Website: </strong>{{$headhunter->company_website}}</label></p>
                        <p><label><strong>Telephone: </strong>{{$headhunter->company_telephone}}</label></p>
                    </div>
                </div>

            </div>
        </div>

        @if(Auth::user()->role == "Job Seekers")
            <div class="mdl-grid apply-card-padding">
                <div class="mdl-cell mdl-cell--1-col"></div>
                <div class="mdl-cell mdl-cell--10-col">
                    @if(Auth::user()->role == "Job Seekers" && $applied != 1)
                        <div class="apply-card">
                            <form class="mdl-btn" action="{{route('applyJob',['jobpost' => $jobpost->id])}}"
                                  method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit"
                                        class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">
                                    Apply
                                </button>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        @endif
    </main>
    @if (session('status'))
        <div class="mdl-js-snackbar mdl-snackbar mdl-snackbar--active">
            <div class="mdl-snackbar__text">
                {{ session('status') }}
            </div>
            <button class="mdl-snackbar__action" type="button"></button>
        </div>
    @endif
</div>
</body>
</html>