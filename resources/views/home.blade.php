<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/search.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <title>Home</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout" id="homePage">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">
                <a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a>
            </span>
            <!-- Search -->
            <div class="mdh-expandable-search mdl-cell--hide-phone">
                <i class="material-icons">search</i>
                <form action="#">
                    <input type="text" placeholder="Search for jobs" size="1" v-model="searchQuery">
                </form>
            </div>
            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link"
                       href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied
                        Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link"
                       href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted
                        Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit
                    Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>

    <!-- Contents -->
    <main class="mdl-layout__content ">
        <div class="mdl-grid grid-padding">
            <!-- Facet Filter -->
            <div class="mdl-cell mdl-cell--3-col">
                {{--<form action="#">--}}
                    <div class="facet-header">
                        <strong>FILTER RESULTS BY:</strong>
                    </div>

                    <!-- Selected Chip -->
                    <div>
                        {{--single selection chip--}}
                        <span class="mdl-chip" v-for="selectedTag in selectedTags">
                            <span class="mdl-chip__text">@{{ selectedTag }}</span>
                        </span>
                    </div>

                    <div class="facet-job-title">
                        <strong>JOB TITLE:</strong>
                        <ul class="demo-list-item mdl-list">
                            {{--single job title list item--}}
                            <li class="mdl-list__item facet-job-title-list" v-for="jobTitle in jobTitles">
                                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect check" :for="jobTitle">
                                <input type="checkbox" class="mdl-checkbox__input" v-model="selectedTags"
                                       name="jobtitle" :id="jobTitle" :value="jobTitle">
                                    <span class=" mdl-checkbox__label">
											@{{ jobTitle }}
                                    </span>
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div class="facet-city">
                        <strong>CITY:</strong>
                        <ul class="demo-list-item mdl-list ">
                            {{--single city list item--}}
                            <li class="mdl-list__item facet-city-list" v-for="state in allStates">
                                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect check" :for="state">
                                    <input type="checkbox" :id="state" name="job_state" :value="state"
                                           class="mdl-checkbox__input" v-model="selectedTags">
                                    <span class=" mdl-checkbox__label">
											@{{ state }}
										</span>
                                </label>
                            </li>

                        </ul>
                    </div>

                    <div class="facet-apply">
                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" @click="searchByTags()">
                            Apply Filter
                        </button>
                    </div>
                {{--</form>--}}


            </div>

            <!-- Card Container -->
            <div class="mdl-cell mdl-cell--6-col">
            <!-- single card -->
                <div class="demo-card-wide mdl-card mdl-shadow--2dp" v-for="jobpost in filterBy(jobposts, searchQuery, 'title', 'state','headhunter_name')">
                    <div class="mdl-card__title">
                        <h2 class="mdl-card__title-text">@{{jobpost.title}}</h2>
                    </div>
                    <div class="mdl-card__supporting-text text-in-card company-name">
                        <a class="company-name" :href="'/HeadhunterProfile/'+ jobpost.headhunter_id">
                            @{{jobpost.headhunter_name}}
                        </a>
                        <h5 class="job-location">
                            <i class="material-icons location-icon">place</i>@{{jobpost.state}}
                        </h5>

                    </div>
                    <p>
                    <div class="mdl-card__actions mdl-card--border">
                        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" :href="'/JobPost/'+jobpost.id">
                        View
                        </a>
                        @if(Auth::user()->role == "Job Seekers")
                            <a class="mdl-button">
                                <form class="mdl-btn" :action="'/JobPost/'+jobpost.id+'/apply'" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                        Apply
                                    </button>
                                </form>
                            </a>
                        @endif
                    </div>
                </div>
                {{--@endforeach--}}
            </div>
        </div>
    </main>
    @if (session('status'))
        <div class="mdl-js-snackbar mdl-snackbar mdl-snackbar--active">
            <div class="mdl-snackbar__text">
                {{ session('status') }}
            </div>
            <button class="mdl-snackbar__action" type="button"></button>
        </div>
    @endif
</div>
<script src="/js/vue.js"></script>
<script src="/js/vue2-filters.min.js"></script>
<script src="/js/axios.min.js"></script>

<script type="text/javascript">
    window.axios.defaults.headers.common = {
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
        'X-Requested-With': 'XMLHttpRequest'
    };

    new Vue({
        el: '#homePage',
        data: {
            jobposts: {!! $jobposts!!},
            allJobposts: {!! $jobposts!!},
            jobTitles: {!! $allTitles!!},
            allStates: {!! $allStates!!},
            searchQuery: "",
            selectedTags: []
        },
        methods: {
            searchByTags: function(){
                if(this.selectedTags.length>0)
                {
                    axios
                        .post('/searchByTags', {
                            selectedTags: this.selectedTags
                        })
                        .then(function (response) {
                            this.jobposts = response.data;
                        }.bind(this))
                        .catch(function (error) {
                            console.log(error);
                        });
                }
                else
                {
                    this.jobposts = this.allJobposts;
                }
            }
        }
    });
</script>
</body>
</html>