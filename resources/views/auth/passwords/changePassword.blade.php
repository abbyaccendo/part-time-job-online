<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/resetPassword.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>

    <script defer src="/js/material.min.js"></script>
    <script defer src="/js/data-required.js"></script>
    <title>Reset Password</title>

</head>
<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">
                <a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a>
            </span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                @if(Auth::user()->role == "Job Seekers")
                    <a class="mdl-navigation__link" href="{{ route('viewMyAppliedJob',['jobseeker' => Auth::user()->jobseeker->id]) }}">View Applied Job</a>
                @else
                    <a class="mdl-navigation__link" href="{{ route('showCreateJobForm') }}">Create Job</a>
                    <a class="mdl-navigation__link" href="{{ route('viewMyPostedJob',['headhunter' => Auth::user()->headhunter->id]) }}">View Posted Job</a>
                @endif
                <a class="mdl-navigation__link" href="{{ route('editProfile',['user' => Auth::user()->id]) }}">Edit Profile</a>
                <a class="mdl-navigation__link" href="{{ route('logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </nav>
        </div>
    </header>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--3-col"></div>
            <div class="mdl-cell mdl-cell--6-col">
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title card__title">
                        <h2 class="mdl-card__title-text">Change Password</h2>
                    </div>
                    <div class="mdl-card__supporting-text text-in-card">
                        <form class="form-horizontal" method="POST" action="{{ route('storeNewPassword') }}">
                            {{ csrf_field() }}

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="password" class="mdl-textfield__label">New Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="mdl-textfield__input" name="password" required>
                                </div>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="password-confirm" class="mdl-textfield__label">Confirm Password</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="mdl-textfield__input"
                                           name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="ubmit-btn">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">
                                        Change Password
                                    </button>
                                </div>
                            </div>
                        </form>

                        @if (count($errors))
                            <div class="alert alert-danger fade in m-b-15">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </main>
    @if (session('status'))
        <div class="mdl-js-snackbar mdl-snackbar mdl-snackbar--active">
            <div class="mdl-snackbar__text">
                {{ session('status') }}
            </div>
            <button class="mdl-snackbar__action" type="button"></button>
        </div>
    @endif
</div>
</body>
</html>
