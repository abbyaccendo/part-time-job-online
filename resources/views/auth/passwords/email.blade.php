<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/resetPassword.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>

    <script defer src="/js/material.min.js"></script>
    <script defer src="/js/data-required.js"></script>
    <title>Reset Password</title>

</head>
<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title"><a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a></span>
            <div class="mdl-layout-spacer"></div>

            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="{{ route('login') }}">Log in</a>
                <a class="mdl-navigation__link" href="{{ route('register') }}">Sign up</a>
            </nav>
        </div>
    </header>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--3-col"></div>
            <div class="mdl-cell mdl-cell--6-col">
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title card__title">
                        <h2 class="mdl-card__title-text">Reset Password</h2>
                    </div>
                    <div class="mdl-card__supporting-text text-in-card">
                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="email" name="email" required>
                                <label class="mdl-textfield__label" for="email">Email</label>
                            </div>

                            <div class="submit-btn">
                                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" type="submit">
                                    Send Password Reset Link
                                </button>
                            </div>

                        </form>

                        @if (count($errors))
                        <div class="alert alert-danger fade in m-b-15">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </main>
    @if (session('status'))
        <div class="mdl-js-snackbar mdl-snackbar mdl-snackbar--active">
            <div class="mdl-snackbar__text">
                {{ session('status') }}
            </div>
            <button class="mdl-snackbar__action" type="button"></button>
        </div>
    @endif
</div>
</body>
</html>

