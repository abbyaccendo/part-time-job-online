<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/signup.css">
    <link rel="stylesheet" href="/css/materialIcons.css">
    <link rel="stylesheet" href="/css/purple.min.css"/>
    <script defer src="/js/material.min.js"></script>
    <link rel="stylesheet" href="/css/getmdl-select.min.css">
    <script defer src="/js/getmdl-select.min.js"></script>
    <script defer src="/js/data-required.js"></script>
    <title>Sign Up</title>

</head>

<body>
<div class="layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title"><a class="title-link" href="{{route('landingPage')}}">Part-time Job Online</a></span>
            <div class="mdl-layout-spacer"></div>
            <!-- menu button -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="{{ route('login') }}">Log in</a>
                <a class="mdl-navigation__link" href="{{ route('register') }}">Sign up</a>
            </nav>
        </div>
    </header>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--3-col"></div>
            <div class="mdl-cell mdl-cell--6-col">
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title card__title">
                        <h2 class="mdl-card__title-text">Sign Up</h2>
                    </div>
                    <div class="mdl-card__supporting-text text-in-card">

                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="name" name="name" required/>
                                <label class="mdl-textfield__label" for="name">Name</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="email" name="email" required/>
                                <label class="mdl-textfield__label" for="email">Email Address</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="tel" pattern="-?[0-9]*(\.[0-9]+)?"
                                       id="telephone" name="telephone" required/>
                                <label class="mdl-textfield__label" for="telephone">Telephone Number</label>
                                <span class="mdl-textfield__error">Input is not a telephone number!</span>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="password" id="password" name="password" required/>
                                <label class="mdl-textfield__label" for="password">Password</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="password" id="password_confirmation"
                                       name="password_confirmation" required/>
                                <label class="mdl-textfield__label" for="password_confirmation">Confirm password</label>
                            </div>

                            <!-- Users-type selection -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth">
                                <input class="mdl-textfield__input" type="text" id="role" name="role" readonly
                                       tabIndex="-1" onchange="userTypeSelection()" required/>
                                <label for="role">
                                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                </label>
                                <label for="role" class="mdl-textfield__label">Role</label>
                                <ul for="role" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                    <li class="mdl-menu__item" data-val="JS">Job Seekers</li>
                                    <li class="mdl-menu__item" data-val="HH">Headhunters</li>
                                </ul>
                            </div>

                            <!-- Job Seekers 1 -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth selection-toggle"
                                 id="js-1">
                                <input class="mdl-textfield__input" type="text" id="gender" name="gender" readonly
                                       tabIndex="-1" >

                                <label for="gender" class="mdl-textfield__label">Gender</label>
                                <ul for="gender" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                    <li class="mdl-menu__item" data-val="M">Male</li>
                                    <li class="mdl-menu__item" data-val="F">Female</li>
                                </ul>
                            </div>

                            <!-- Job Seekers 2-->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label selection-toggle"
                                 id="js-2">
                                <input class="mdl-textfield__input" type="date" id="dob" name="dob"
                                       placeholder="dd/mm/yyyy" date-format="dd/mm/yyyy" lang="en" >
                                <label class="mdl-textfield__label" for="dob">Date of Birth</label>
                            </div>

                            <!-- Job Seekers 3 -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label selection-toggle" id="js-3">
                                <textarea class="mdl-textfield__input" type="text" rows= "4" id="experience" name="experience"></textarea>
                                <label class="mdl-textfield__label" for="experience">Experience</label>
                            </div>

                            <!-- Headhunters 1 -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label selection-toggle"
                                 id="hh-1">
                                <input class="mdl-textfield__input" type="text" id="company-website" name="company-website" >
                                <label class="mdl-textfield__label" for="company-website">Company Website</label>
                            </div>

                            <!-- Headhunters 2 -->
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label selection-toggle"
                                 id="hh-2">
                                <input class="mdl-textfield__input" type="text" id="company-address" name="company-address" >
                                <label class="mdl-textfield__label" for="company-address">Company Address</label>
                            </div>

                            <!-- Signup button -->
                            <div class="signup-apply">
                                {{--<div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" type="submit" onclick="submitValidation()">--}}
                                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary"
                                        type="submit">
                                    Sign Up
                                </button>
                                {{--</div>--}}

                            </div>

                        </form>

                        @if (count($errors))
                            <div class="alert alert-danger fade in m-b-15">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="/js/date-input-polyfill.js"></script>

<script>
    // Validation function
    //need to do complete form function also
    // function submitValidation() {
    //     if (document.getElementById("name").value != '') {
    //         alert("HEL");
    //     }
    //     else {
    //         var d = document.getElementById("name");
    //         d.className += " is-invalid";
    //     }
    // }

    //Users-type Selection Show/Hide
    function userTypeSelection() {
        var x = document.getElementById("role").value;
        if (x === 'Headhunters') {
            document.getElementById("js-1").style.display = 'none';
            document.getElementById("js-2").style.display = 'none';
            document.getElementById("js-3").style.display = 'none';
            document.getElementById("hh-1").style.display = 'block';
            document.getElementById("hh-2").style.display = 'block';

        }
        else {
            document.getElementById("js-1").style.display = 'block';
            document.getElementById("js-2").style.display = 'block';
            document.getElementById("js-3").style.display = 'block';
            document.getElementById("hh-1").style.display = 'none';
            document.getElementById("hh-2").style.display = 'none';
        }
    }

</script>
</body>
</html>